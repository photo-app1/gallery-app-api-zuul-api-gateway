package com.mobileapp.photoappapizuulapigateway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final Environment env;

    @Autowired
    public WebSecurity(Environment env) {
        this.env = env;
    }

    /***
     * overwrite the configure method which will give us access to HttpSecurity object which will need to configure.
     * @param http HttpSecurity object
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, env.getProperty("api.signup.url.path")).permitAll()
                .antMatchers(HttpMethod.POST, env.getProperty("api.login.url.path")).permitAll()
                .antMatchers(env.getProperty("api.auth-service.h2console.url.path")).permitAll()
                .antMatchers(env.getProperty("api.albums-ws.h2console.url.path")).permitAll()
                .antMatchers(env.getProperty("api.actuator.url.path")).permitAll()
                .antMatchers(env.getProperty("api.auth-service.actuator.url.path")).permitAll()
                .antMatchers(env.getProperty("api.albums-ws.actuator.url.path")).permitAll()
                .antMatchers(env.getProperty("api.auth-service.admin-role.path")).hasRole("ROLE_ADMIN")
                .antMatchers(env.getProperty("api.auth-service.management-role.path")).hasAnyRole("ROLE_ADMIN", "ROLE_MANAGEMENT")
                .anyRequest().authenticated()
                .and()
                //register AuthorizationFilter with web security
                .addFilter(new AuthorizationFilter(authenticationManager(), env));

        //make our Rest API stateless.(tell spring to not to create http sessions when client app communicate with
        // server side because this sessions & cookies can cache some information about request(authorization header/
        // JWT token). after following requests will still be authorize without authorize header(JWT Token).
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }
}