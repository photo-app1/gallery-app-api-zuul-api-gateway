package com.mobileapp.photoappapizuulapigateway.security;

import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AuthorizationFilter extends BasicAuthenticationFilter {

    private Environment env;

    @Autowired
    public AuthorizationFilter(AuthenticationManager authenticationManager, Environment env) {
        super(authenticationManager);
        this.env = env;
    }

    /***
     * check token & prefix exist in authorization header, then call token validate method getAuthentication to
     * perform authorization. this method will be called by spring framework.
     * @param request HttpServletRequest object
     * @param response HttpServletResponse object
     * @param chain FilterChain object
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        //reading the value which is included in the authorization http header
        String authorizationHeader = request.getHeader(env.getProperty("authorization.token.header.name"));

        //if authorizationHeader is empty or it doesn't start with Bearer prefix then simply continuing
        // and passing on execution to the next filter in the chain.
        if (authorizationHeader == null || !authorizationHeader.startsWith(env.getProperty(
                "authorization.token.header.prefix"))) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    /***
     * validate provided JWT token
     * @param request HttpServletRequest object
     * @return UsernamePasswordAuthenticationToken object with userId & new Arraylist
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {

        String authorizationHeader = request.getHeader(env.getProperty("authorization.token.header.name"));

        if (authorizationHeader == null) {
            return null;
        }

        // remove Bearer prefix & parse out userId (which was encoded into token) from token / validate token
        String token = authorizationHeader.replace(env.getProperty("authorization.token.header.prefix"), "");
        String userId = Jwts.parser()
                .setSigningKey(env.getProperty("token.secret.key"))
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        if (userId == null) {
            return null;
        }

        return new UsernamePasswordAuthenticationToken(userId, null, new ArrayList<>());
    }

}