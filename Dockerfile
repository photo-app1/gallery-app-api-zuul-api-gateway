FROM openjdk:11
VOLUME /tmp
COPY build/libs/gallery-app-api-zuul-api-gateway-0.0.1-SNAPSHOT.jar GalleryAppApiZuulApiGateway.jar
ENTRYPOINT ["java","-jar","GalleryAppApiZuulApiGateway.jar"]